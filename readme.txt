Andres Felipe Huertas Suarez	
"Full Dark, no stars"

Este repositorio contiene los codigos que utilizare a lo largo de mi trabajo de tesis con el grupo de simulacion de sistemas fisicos de la universidad nacional de colombia. 

Se encontraran codigos en c++/python para resolver problemas de fluidos, con especial interes en desarrollar un framework completo en cuda que permita resolver problemas en general. Este respositorio se da por iniciado el dia 29 de Agosto de 2015. 


--- SOME FIRST RESULTS ---- 

hoy 13 de septiembre se temino con exito la primera implementacion de los scripts en CUDA, CUDA/ArrayFire , y c++ puro, los resultados de performance estan aqui, la medida de performance sera Lattices procesados por segundo 

CUDA : 1544 Lattices/s
ArrayFire : 225 Lattices/s  ( 6 veces mas lento que CUDA puro )
C++ : 134 Lattices/s ( 11 veces mas lento que CUDA ) 


------ 23 de octubre -----

El modulo que carga los modelos 3d se encuentra funcionando ( con limitaciones que describire mas adelante) en la carpeta ./c++/jc++/Assimp/meshModule/ la compilacion la realiza un script de cmake. 

por el momento la cosa parece funcionar de forma apropiada con ciertas limitaciones. Los objetos NO PUEDEN TENER AGUJEROS es decir, un cono, una esfera un cilindro son apropiadamente descritos, pero un toro por ejemplo no. El programa rellena estos agujeros y esto es consecuencia posiblemente del tipo de estrucutra utilizada por el programa de la CGAL ( los Polyhedros tienen ciertas restricciones que aun no he estudiado en profundidad ) .

Por el momento, el archivo .obj solo puede dar cuenta de 1 mesh, sin embargo para continuar con el espiritu del grupo dejare un momento el problema geometrico y me enfocare en la implementacion en 3D en ArrayFire del lattice. 

 
