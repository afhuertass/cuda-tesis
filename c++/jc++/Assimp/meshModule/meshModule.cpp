#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/Polyhedral_mesh_domain_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/refine_mesh_3.h>
#include <CGAL/Triangulation_3.h>

#include <string>
#include <vector>
#include <iostream>
#include <fstream>


#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


typedef CGAL::Simple_cartesian<double>     Kernel;
typedef CGAL::Polyhedron_3<Kernel>         Polyhedron;
typedef Polyhedron::HalfedgeDS             HalfedgeDS;

typedef CGAL::Simple_cartesian<double>               Kernel;
typedef Kernel::Point_3                              Point_3;

typedef Polyhedron::Facet_iterator                   Facet_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;

// definiciones mesh.cpp 
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K> Polyhedron_m;
typedef CGAL::Polyhedral_mesh_domain_3<Polyhedron_m, K> Mesh_domain;
typedef CGAL::Triangulation_3<K> Triangulation_3;

#ifdef CGAL_CONCURRENT_MESH_3
typedef CGAL::Mesh_triangulation_3<
  Mesh_domain,
  CGAL::Kernel_traits<Mesh_domain>::Kernel, // Same as sequential
  CGAL::Parallel_tag                        // Tag to activate parallelism
  >::type Tr;
#else
typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr;
#endif
typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;
// Criteria
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;
typedef C3t3::Cell_handle Cell_handle;
typedef Tr::Locate_type Locate_type;
typedef Triangulation_3::Point Point;
/// fin definiciones mesh.cpp


///    CLASES DE EL POLY.CPP
template <class HDS>
class Polyhedron_builder : public CGAL::Modifier_base<HDS>{
public:
  std::vector<double> &coords;
  std::vector<aiFace> &faces;
  Polyhedron_builder( std::vector<double> &c , std::vector<aiFace> &f) : coords(c) , faces(f){}
  void operator()(HDS& hds) {
    typedef typename HDS::Vertex Vertex;
    typedef typename Vertex::Point Point;
    CGAL::Polyhedron_incremental_builder_3<HDS> B(hds,false);
    
    B.begin_surface(coords.size()/3, faces.size()  );
    //  B.begin_surface( vertices , caras esperadas, h  );
    for(int i = 0; i < (int)coords.size() ; i+=3 ){
      B.add_vertex( Point( coords[i] , coords[i+1] , coords[i+2] ));
    }
    
    for ( int i = 0; i < (int) faces.size(); i+=1){
      B.begin_facet(); // Iteramos por cada cara agregando los indicesa el facet
      for ( int j = 0 ; j < faces[i].mNumIndices; j++) {
	B.add_vertex_to_facet( faces[i].mIndices[j] );
      } 
      B.end_facet(); // fin de la cara 
    }
    B.end_surface(); // construimos la superficie 
  }
  
  
};

class PolyhedronContainer{
public:
  Polyhedron P; // para guardar el resultado.
  
  PolyhedronContainer(aiMesh* oMesh); // constructor, recibimos una mesh de la Scene.
  
  Polyhedron getPoly();
private:
  std::vector<double> coords;
  std::vector<aiFace> faces;
  
};

PolyhedronContainer::PolyhedronContainer(aiMesh* oMesh){
  // en el constructor vamos a procesar la mesh ... llenar los vectores de coordenadas y generar el polyhedro de una buena puta vez. 

  for(int i = 0;  i < oMesh->mNumVertices; i++){
    coords.push_back(oMesh->mVertices[i].x );
    coords.push_back(oMesh->mVertices[i].y );
    coords.push_back(oMesh->mVertices[i].z );
    
  }
  // ahora guardar las caras en un arreglo
  
  for( int i = 0; i < oMesh->mNumFaces; i++){
    // por cada cara
    aiFace face = oMesh->mFaces[i];
    faces.push_back(face);
    
  }
  Polyhedron_builder<HalfedgeDS> builder(coords, faces);
  P.delegate(builder);
  
}
Polyhedron PolyhedronContainer::getPoly(){
  return this->P;
}
class Modelo {
  // think el modelo va a tener los polyedros 
public:
  std::vector<Polyhedron> polys;
  Modelo(std::string path); // constructor, recibe la ruta para cargar el modelo 
  void getOff();
  Polyhedron_m createPolyForMesh();
private:
  void loadModelo(std::string path);
  void populatePolys(aiNode* nodo ,const aiScene* scene);
};

Modelo::Modelo(std::string path){
  
  this->loadModelo(path);
  
}
void Modelo::loadModelo(std::string path){
   Assimp::Importer importer;
   const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
   if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
     {
       std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
       return;
     }
   // en esta parte el modelo esta cargado. y guardado en scene. debemos obteneer las mesh

   this->populatePolys(scene->mRootNode ,scene);
   
}
void Modelo::populatePolys(aiNode* node, const aiScene* scene){
  // esta cosa tiene que funcionar recursicamente.
  for ( int i = 0; i < node->mNumMeshes; i++){
    // iterar sobre todas las mesh del nodo actual
    aiMesh* mesh = scene->mMeshes[node->mMeshes[i] ];
    PolyhedronContainer polC(mesh);
    polys.push_back(  polC.getPoly()  ); // un arreglo de los pol
  }
  
  for( int i = 0; i < node->mNumChildren; i++){
    
    this->populatePolys(node->mChildren[i] , scene );
    
  }
  
}
void Modelo::getOff(){
  // sacar archivos .off
  std::ofstream os;
  os.open("out.off");
  
  for(int i = 0; i < polys.size() ; i++){
    Polyhedron P = polys.at(0);
  
    os << P;
  }
  os.close();
}
Polyhedron_m Modelo::createPolyForMesh(){
  Polyhedron_m p;
  this->getOff();
  std::ifstream input("out.off");
  input >> p;
  return p;
  
}
// FIN CLASES POLY.CPP

using namespace CGAL::parameters;

/// comienzo clases mesh.cpp 
class MeshHelper{
  // clase para generar Mesh3D a partir de polyhedro
public:
  MeshHelper(Polyhedron_m p);
  void saveMeshFile();
  void saveBoundary();
  bool isPointIn(const Point &p );
private:
  C3t3 mesh;
  
  void generateMesh(Polyhedron_m p);
  
  
};

MeshHelper::MeshHelper(Polyhedron_m p){
  // en el constructor generamos el mesh
  this->generateMesh(p);
  
    
}
void MeshHelper::generateMesh(Polyhedron_m p){
  Mesh_domain domain(p);
  Mesh_criteria criteria(facet_angle=25, facet_size=0.15, facet_distance=0.008,cell_radius_edge_ratio=3);
  // criterios, para ensayos, probamos con estos y luego extendemos.

  this->mesh = CGAL::make_mesh_3<C3t3>( domain , criteria , no_perturb(), no_exude()) ;
  

}
void MeshHelper::saveMeshFile(){
  
  std::ofstream medit_file("out.mesh");
  this->mesh.output_to_medit(medit_file);
  medit_file.close();

};
void MeshHelper::saveBoundary(){
  std::ofstream off_file("boundary.off");
  this->mesh.output_boundary_to_off(off_file);
  off_file.close();
  
}
bool MeshHelper::isPointIn(const Point  &query){
  // 
  Locate_type lt;
  int ii, jj;
  Tr &tr = mesh.triangulation();
  //Cell_handle infinite = tr.infite_cell();
  Cell_handle ch = tr.locate( query , lt , ii ,jj );
  if(lt == Tr::CELL && !tr.is_infinite(ch ) ){
    return true;
  }else {
    return false;
  }
 
}

/// fin clases Mesh 
int main(){

  Modelo casa("cono.obj");
  Polyhedron_m p = casa.createPolyForMesh();
  MeshHelper mh( p );
  mh.saveMeshFile();
  Point punto(0,0,0);
  int rr = 0;
  int grid[50][50][50]; // grilla de simulacion 50x50x50 celdas
  // digamos que el cubo esta en un espacio de 4x4x4 metros
  // intervalo -2 hasta 2  
  // 1 celda = 0.04 metros
  double x0, y0, z0; // conejo -7e-2  7e-2
  double ini = -2, fini = 2;
  x0 = y0 = z0 = ini;
  double deltaM = (fini-ini)/50; 
  for( int ix = 0 ; ix < 50 ; ix++ ,x0 +=deltaM){
    for(int iy = 0 ; iy < 50 ; iy++, y0 += deltaM){
      for(int iz = 0; iz < 50 ; iz++ , z0+= deltaM){
	Point punto(x0,y0,z0);
	//std::cout << x0 << " " << y0  <<" " << z0  <<  std::endl;
	if ( mh.isPointIn(punto)){
	  grid[ix][iy][iz] = 121;
	  std::cout << x0 << " " << y0  <<" " << z0  << " " << grid[ix][iy][iz] << std::endl;
	}else {
	  grid[ix][iy][iz] = 0;
	}
	//std::cout << x0 << " " << y0  <<" " << z0  << " " << grid[ix][iy][iz] << std::endl;
      }
      z0 = ini;
    }
    y0 = ini;
    std::cout << std::endl;
  }
  for( int ix = 0 ; ix < 50 ; ix++){
    for(int iy = 0 ; iy < 50 ; iy++){ 
      for(int iz = 0; iz < 50 ; iz++){ 
	//std::cout << ix << " " << iy  <<" " << iz  << " " << grid[ix][iy][iz] << std::endl;
	
      } 
    
    }
    //std::cout << std::endl;
  }
  // if( mh.isPointIn(punto)) rr = 10;
  //std::cout << rr << std::endl;
  //mh.saveMeshFile();
  return 0;
}
