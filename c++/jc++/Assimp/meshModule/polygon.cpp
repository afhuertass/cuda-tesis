#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>

#include <string>
#include <vector>
#include <iostream>
#include <fstream>


#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


typedef CGAL::Simple_cartesian<double>     Kernel;
typedef CGAL::Polyhedron_3<Kernel>         Polyhedron;
typedef Polyhedron::HalfedgeDS             HalfedgeDS;

typedef CGAL::Simple_cartesian<double>               Kernel;
typedef Kernel::Point_3                              Point_3;

typedef Polyhedron::Facet_iterator                   Facet_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;

template <class HDS>
class Polyhedron_builder : public CGAL::Modifier_base<HDS>{
public:
  std::vector<double> &coords;
  std::vector<aiFace> &faces;
  Polyhedron_builder( std::vector<double> &c , std::vector<aiFace> &f) : coords(c) , faces(f){}
  void operator()(HDS& hds) {
    typedef typename HDS::Vertex Vertex;
    typedef typename Vertex::Point Point;
    CGAL::Polyhedron_incremental_builder_3<HDS> B(hds,false);
    
    B.begin_surface(coords.size()/3, faces.size()  );
    //  B.begin_surface( vertices , caras esperadas, h  );
    for(int i = 0; i < (int)coords.size() ; i+=3 ){
      B.add_vertex( Point( coords[i] , coords[i+1] , coords[i+2] ));
    }
    
    for ( int i = 0; i < (int) faces.size(); i+=1){
      B.begin_facet(); // Iteramos por cada cara agregando los indicesa el facet
      for ( int j = 0 ; j < faces[i].mNumIndices; j++) {
	B.add_vertex_to_facet( faces[i].mIndices[j] );
      } 
      B.end_facet(); // fin de la cara 
    }
    B.end_surface(); // construimos la superficie 
  }
  
  
};

class PolyhedronContainer{
public:
  Polyhedron P; // para guardar el resultado.
  
  PolyhedronContainer(aiMesh* oMesh); // constructor, recibimos una mesh de la Scene.
  
  Polyhedron getPoly();
private:
  std::vector<double> coords;
  std::vector<aiFace> faces;
  
};

PolyhedronContainer::PolyhedronContainer(aiMesh* oMesh){
  // en el constructor vamos a procesar la mesh ... llenar los vectores de coordenadas y generar el polyhedro de una buena puta vez. 

  for(int i = 0;  i < oMesh->mNumVertices; i++){
    coords.push_back(oMesh->mVertices[i].x );
    coords.push_back(oMesh->mVertices[i].y );
    coords.push_back(oMesh->mVertices[i].z );
    
  }
  // ahora guardar las caras en un arreglo
  
  for( int i = 0; i < oMesh->mNumFaces; i++){
    // por cada cara
    aiFace face = oMesh->mFaces[i];
    faces.push_back(face);
    
  }
  Polyhedron_builder<HalfedgeDS> builder(coords, faces);
  P.delegate(builder);
  
}
Polyhedron PolyhedronContainer::getPoly(){
  return this->P;
}
class Modelo {
  // think el modelo va a tener los polyedros 
public:
  std::vector<Polyhedron> polys;
  Modelo(std::string path); // constructor, recibe la ruta para cargar el modelo 
  void getOff();
private:
  void loadModelo(std::string path);
  void populatePolys(aiNode* nodo ,const aiScene* scene);
};

Modelo::Modelo(std::string path){
  
  this->loadModelo(path);
  
}
void Modelo::loadModelo(std::string path){
   Assimp::Importer importer;
   const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
   if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
     {
       std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
       return;
     }
   // en esta parte el modelo esta cargado. y guardado en scene. debemos obteneer las mesh

   this->populatePolys(scene->mRootNode ,scene);
   
}
void Modelo::populatePolys(aiNode* node, const aiScene* scene){
  // esta cosa tiene que funcionar recursicamente.
  for ( int i = 0; i < node->mNumMeshes; i++){
    // iterar sobre todas las mesh del nodo actual
    aiMesh* mesh = scene->mMeshes[node->mMeshes[i] ];
    PolyhedronContainer polC(mesh);
    polys.push_back(  polC.getPoly()  ); // un arreglo de los pol
  }
  
  for( int i = 0; i < node->mNumChildren; i++){
    
    this->populatePolys(node->mChildren[i] , scene );
    
  }
  
}
void Modelo::getOff(){
  // sacar archivos .off
  std::ofstream os;
  os.open("out.off");
  
  for(int i = 0; i < polys.size() ; i++){
    Polyhedron P = polys.at(i);
    /* std::cout << "OFF" << std::endl << P.size_of_vertices() << ' '
              << P.size_of_facets() << " 0" << std::endl;
    std::copy( P.points_begin(), P.points_end(),
               std::ostream_iterator<Point_3>( std::cout, "\n"));
    for (  Facet_iterator i = P.facets_begin(); i != P.facets_end(); ++i) {
      Halfedge_facet_circulator j = i->facet_begin();
      // Facets in polyhedral surfaces are at least triangles.
      CGAL_assertion( CGAL::circulator_size(j) >= 3);
      std::cout << CGAL::circulator_size(j) << ' ';
      do {
	std::cout << ' ' << std::distance(P.vertices_begin(), j->vertex());
      } while ( ++j != i->facet_begin());
      std::cout << std::endl;
    }

*/
    os << P;
  }
  os.close();
}
int main() {

  Modelo cubo("./Audi1.obj");
  cubo.getOff();
  return 0;
}


/*
  
 */



