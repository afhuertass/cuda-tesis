#include <arrayfire.h>
#include "2latticed3q15.h"
#include <vector>

// Modelo 3d para lattice bolztman utlizando el metodo entrpico 
using namespace af;
LatticeBolztmannD3Q15::LatticeBolztmannD3Q15(int lx,int ly, int lz){
  //constructor
  this->q = 15;
  this->Lx=lx; // tamaño del lattice
  this->Ly=ly;
  this->Lz=lz;
  fs.resize(q);
  for( int i = 0 ; i < q ; i++){
    this->fs.at(i) = af::constant(0,Lx,Ly,Lz , f32);
  }
  w = af::constant( 0 , q , f32);
  w(0) = 16; w(1)=w(2)=w(3)=w(4)=w(5)=w(6) = 8;
  w(7)=w(8)=w(9)=w(10)=w(11)=w(12)=w(13)=w(14)=1;
  w = 1/72.0*w;
  W[0] = 16.0/72;
  W[1] = W[2] = W[3] =   W[4] = W[5] = W[6] = 8.0/72;
  W[7] = W[8] = W[9] =   W[10] = W[11] = W[12] = W[13] = W[14] = 1.0/72; 
  
  V[0][0] = 0; V[0][1] = 1; V[0][2] = 0; V[0][3] = 0 ; V[0][4] = -1;
  V[0][5] = 0; V[0][6] = 0; V[0][7] = 1; V[0][8] = -1 ; V[0][9] = 1;
  V[0][10] = 1; V[0][11] = -1; V[0][12] = 1; V[0][13] = -1 ; V[0][14] = -1;
  
  V[1][0] = 0; V[1][1] = 0; V[1][2] = 1; V[1][3] = 0 ; V[1][4] = 0;
  V[1][5] = -1; V[1][6] = 0; V[1][7] = 1; V[1][8] = 1 ; V[1][9] = -1;
  V[1][10] = 1; V[1][11] = -1; V[1][12] = -1; V[1][13] = 1 ; V[1][14] = -1;

  V[2][0] = 0; V[2][1] = 0; V[2][2] = 0; V[2][3] = 1 ; V[2][4] = 0;
  V[2][5] = 0; V[2][6] = -1; V[2][7] = 1; V[2][8] = 1 ; V[2][9] = 1;
  V[2][10] = -1; V[2][11] = 1; V[2][12] = -1; V[2][13] = -1 ; V[2][14] = -1;

  float Re = 100 ; // numero de Reynolds
  float viscosidad = 1/Re;
  float tau = 3*(viscosidad);
  
  float dx = 1/Lx;
  float dt = dx*dx;

  this->beta = dt/(2*tau +dt);

}
void LatticeBolztmannD3Q15::Inicie(float r0 , float Ux0 , float Uy0 , float Uz0){
  array rh0 = af::constant( r0 , Lx, Ly, Lz , f32);
  array Uxs = af::constant( Ux0 , Lx , Ly , Lz , f32);
  array Uys = af::constant( Uy0 , Lx , Ly , Lz , f32);
  array Uzs = af::constant( Uz0 , Lx , Ly , Lz , f32);

  //f = this->feq( rh0, Uxs , Uys, Uzs);
  int i = 0;
  for( i = 0; i< q ; i++ ) {
    this->fs.at(i) = this->feq2(i,rh0 , Uxs, Uys, Uzs ) ;
  }
  //af_print(this->Jx() );
}
array LatticeBolztmannD3Q15::feq(int i ,  array &rhos, array &Uxs , array &Uys , array &Uzs){
  // array feq(Lx,Ly,Lz, f32);
  array chi = this->chi( rhos, Uxs, Uys, Uzs);
    //w(i)*chi*zita_a(Uxs)*zita_a(Uys)*zita_a(Uzs);
  
  // return ( w(0)*chi*zita_a(Uxs)*zita_a(Uys)*zita_a(Uzs)    );
  return (W[i]*chi*af::pow(zita_a(Uxs), V[0][i])*af::pow(zita_a(Uys), V[1][i])*af::pow(zita_a(Uzs), V[2][i]) )  ;
}
array LatticeBolztmannD3Q15::feq2(int i ,  array &rhos, array &Uxs , array &Uys , array &Uzs){ 
  // 0 x , 1 y , 2 , z 
  return rhos*W[i]*aux(i,0, Uxs)*aux(i,1,Uys)*aux(i,2,Uzs);
}

array LatticeBolztmannD3Q15::rho(){
  array f = af::constant(0, Lx, Ly,Lz , f32 );
  
  //return (af::sum(f,3)); // suma a lo largo de la tercera componente
  for( int i = 0 ; i < q ; i++){
    f += this->fs.at(i);
  }
  return f;
}
array LatticeBolztmannD3Q15::Jx(){
  array Jx = af::constant(0 , Lx, Ly,Lz, f32);
  
  Jx += fs.at(1) -fs.at(4) + fs.at(7)-fs.at(8)+fs.at(9)+fs.at(10)-fs.at(11)+fs.at(12)-fs.at(13)-fs.at(14) ;
  
  return Jx;
}
array LatticeBolztmannD3Q15::Jy(){
  array Jy = af::constant(0 , Lx, Ly,Lz, f32);
  Jy += fs.at(2)-fs.at(5)+fs.at(7)+fs.at(8)-fs.at(9)+fs.at(10)-fs.at(11)-fs.at(12)+fs.at(13)-fs.at(14); 
  return Jy;
}
array LatticeBolztmannD3Q15::Jz(){
 array Jz = af::constant(0 , Lx, Ly,Lz, f32);
 Jz+= fs.at(3)-fs.at(6)+fs.at(7)+fs.at(8)+fs.at(9)-fs.at(10)+fs.at(11)-fs.at(12)-fs.at(13)-fs.at(14); 
  return Jz;
}
array LatticeBolztmannD3Q15::chi( array &rho, array &Ux , array &Uy , array &Uz){
  array U2 = af::pow(Ux,2) + af::pow(Uy,2) + af::pow(Uz,2);
  array II = af::constant(1 , Lx,Ly,Lz , f32);

  return (rho*(II-3.0/2*U2+9.0/8*af::pow(U2,2 ) + chi6(Ux,Uy,Uz ) + chi8(Ux ,Uy ,Uz) ) );
  
}
array LatticeBolztmannD3Q15::chi6( array &Ux , array &Uy , array &Uz){
  array U2 = af::pow(Ux,2) + af::pow(Uy,2) + af::pow(Uz,2);
  array Ux2 = af::pow(Ux,2); array Uy2 = af::pow(Uy,2); array Uz2 = af::pow(Uz,2);
  return ( 27.0/16*(-af::pow(U2,3)+2*(Uy2 +Uz2 )*(U2*Ux2 + Uy2*Uz2 )+20*Ux2*Uy2*Uz2 ) );
}
array LatticeBolztmannD3Q15::chi8( array &Ux , array &Uy , array &Uz){
  array U2 = af::pow(Ux,2) + af::pow(Uy,2) + af::pow(Uz,2);
  array Ux2 = af::pow(Ux,2); array Uy2 = af::pow(Uy,2); array Uz2 = af::pow(Uz,2);
  array Ux4 = af::pow(Ux,4); array Uy4 = af::pow(Uy,4); array Uz4 = af::pow(Uz,4);
  array Ux8 = af::pow(Ux2,4); array Uy8 = af::pow(Uy2,4) ; array Uz8 = af::pow(Uz2,4);
  return ( 81.0/128*af::pow(U2,4) + 81.0/32*(Ux8+Uy8+Uz8-36*Ux2*Uy2*Uz2*U2-Ux4*Uy4-Uy4*Uz4-Ux4*Uz4 ) );
}
array  LatticeBolztmannD3Q15::aux(int i,int alpha, array &Ua){
  array u = af::constant(1, Lx,Ly,Lz, f32);
  array U2 = af::pow(Ua,2);
  array term1 =  (2*u-af::sqrt( 1+ 3*U2 ) );
  if ( V[alpha][i] == 0 ){
    return term1;
  }else {
    array term2 = af::pow((2*Ua+af::sqrt(1+3*U2 ) )/(u-Ua) , V[alpha][i]);
    return term1*term2;
  }
		  
 
}
array LatticeBolztmannD3Q15::zita_a(array &Ua){
    array unity = af::constant(1 , Lx,Ly,Lz , f32);
    return ( unity + 3*Ua +9.0/2*af::pow(Ua,2) + 9.0/2*af::pow(Ua,3)+27.0/8*af::pow(Ua,4));
}

void LatticeBolztmannD3Q15::Colission(){
  float alpha=2;
  array rho = this->rho();
  array Ux = this->Jx()/rho ;
  array Uy = this->Jy()/rho;
  array Uz = this->Jz()/rho;
  //f +=  alpha*beta*( feq(rho,Ux,Uy,Uz ) - f  );
  for( int i = 0; i < q ; i++){
    fs.at(i) += alpha*beta*( feq2( i, rho,  Ux , Uy , Uz ) - fs.at(i)); 
  }
}

void LatticeBolztmannD3Q15::Adveccion(){
  
  for(int i = 0; i< q ; i++){
    this->fs.at(i) = shift( this->fs.at(i) , V[1][i] , V[0][i] , V[2][i]  ) ;
    //af_print(fs.at(i));
  }

}
void LatticeBolztmannD3Q15::Print(void){
  array rho = this->rho();
  array Ux = this->Jx()/rho; 
  array Uy = this->Jy()/rho;
  array Uz = this->Jz()/rho;
  
  float *h_ux = Ux.host<float>();
  float *h_uy = Uy.host<float>();
  float *h_uz = Uz.host<float>();
  int z = 10;
  for(int x = 0 ; x < Lx ; x++)
    for(int y = 0 ; y< Ly;y++)
      //for(int z = 0 ; z < Lz ; z++)
	std::cout << x << " " << y << " "<< 20*h_ux[x + Ly*(y + Lx*z) ] << " " <<20*h_uy[x + Ly*(y + Lx*z) ] << " " << h_uz[x + Ly*(y + Lx*z) ] <<std::endl;
  //af::free(h_ux);
  
}
int main(int argc , char *argv[] ){
  int pasos = 1000;
  int device = argc > 1 ? atoi(argv[1]) : 0 ;
  af::setDevice(device);
  //af::info();
  int Lx, Ly , Lz;
  Lx = Ly = Lz = 50;
  double memory_size = (Lx*Ly*Lz*15);
  std::cout << "#Memoria requerida MB:" << memory_size/1e6 << std::endl;
  std::cout << "# vectores" << std::endl;
  LatticeBolztmannD3Q15 fluid(Lx,Ly,Lz);
  fluid.Inicie( 1.0 , 0.1 , 0.0 , 0.0);
  timer reloj = timer::start();
  for ( int i = 0 ; i< pasos ; i++){
    fluid.Colission();
    fluid.Adveccion();
  }
  double t = af::timer::stop(reloj);
  std::cout << "#Tiempo:" << t << std::endl;
  std::cout << "#LUPS  " << (pasos*Lx*Ly*Lz)/t << std::endl;
  fluid.Print();
  return 0;
}
