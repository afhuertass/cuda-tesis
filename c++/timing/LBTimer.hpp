#include <chrono>


class LBTimer{
 private:
  int Lx,Ly, Lz = 1; // Lz por defecto es 1
  int steps=1;
  bool isLattice = true;
  std::chrono::steady_clock::time_point start; 
public:
  
  LBTimer(int lx , int ly , int lz , int steps); // 3d lattices 
  LBTimer(int lx, int ly , int steps);  // 2d lattices
  LBTimer(); // just timing;
  void startTiming(); 
  double stopTiming(); // detiene el tiempo y retorna los LUPS

};

LBTimer::LBTimer(){
  this->isLattice = false;
  
}
LBTimer::LBTimer(int lx, int ly, int lz , int s){
  this->Lx = lx; this->Ly = ly;
  this->Lz = lz;
  this->steps = s;
}
LBTimer::LBTimer(int lx, int ly, int s){
  this->Lx = lx; this->Ly = ly;
  this->steps = s;
}

void LBTimer::startTiming(){
  start = std::chrono::steady_clock::now();
}
double LBTimer::stopTiming(){
  auto end = std::chrono::steady_clock::now();
  
  auto diff = end - start;
  
  double time = std::chrono::duration <double , std::ratio< 1,1 >> (diff).count();
  // time tiempo en segundos, ahora LUPS = Lx*Ly*Lz/t;
  if (! isLattice){
    return time;
  } else {
    double LUPS = (Lx*Ly*Lz*steps)/time;
    return LUPS;
  } 
  
}
